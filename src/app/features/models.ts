export interface Video {
  id: number;
  width: number;
  height: number;
  url: string;
}
